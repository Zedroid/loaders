module.exports = function (input, replacement = '_') {
  return input.toString().replace(/[\\/:*?"<>|]/g, replacement)
}
