// Show header
console.log('XKCD Downloader')
console.log('')

// Create directory
require('../tools/directory')('xkcd')

// Get latest comic ID
console.log('Retrieving latest comic ID...')
console.log('')
const { num } = require('../tools/request')('https://xkcd.com/info.0.json', true)

// Do for each comic
for (let i = 0; i < num; i++) {
  const response = require('../tools/request')('https://xkcd.com/' + (i + 1) + '/info.0.json', true)
  console.log('Downloading ' + response.num + '/' + num + ' (' + response.title + ')...')

  // Download image file
  require('../tools/download')(response.img, response.num + require('../tools/extension')(response.img))
}
