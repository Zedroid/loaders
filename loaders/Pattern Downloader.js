/* eslint-disable no-eval, no-undef */

// Show header
console.log('Pattern Downloader')
console.log('')

// Create directory
require('../tools/directory')('The Pattern Library')

// Get pattern list
console.log('Getting pattern list...')
console.log('')
const response = require('../tools/request')('https://thepatternlibrary.com/js/pattern-library.js')
require('fs').writeFileSync('pattern-library.js', response)

eval(response.match(/var imageDir = .*var loaded = /s).toString().substr(0, response.match(/var imageDir = .*var loaded = /s).toString().length - 13))

// Do for each pattern
console.log('Downloading patternData...')
console.log('')

for (let i = 0; i < patternData.length; i++) {
  console.log('Downloading ' + (i + 1) + '/' + patternData.length + ' (' + patternData[i].name + ')...')

  // Download image file
  require('../tools/download')('https://thepatternlibrary.com/' + imageDir + '/' + patternData[i].file, patternData[i].name + require('../tools/extension')(patternData[i].file), 'patternData', undefined, false)
}

// Do for each slow pattern
console.log('')
console.log('Downloading slowPatternData...')
console.log('')

for (let i = 0; i < slowPatternData.length; i++) {
  console.log('Downloading ' + (i + 1) + '/' + slowPatternData.length + ' (' + slowPatternData[i].name + ')...')

  // Download image file
  require('../tools/download')('https://thepatternlibrary.com/' + imageDir + '/' + slowPatternData[i].file, slowPatternData[i].name + require('../tools/extension')(slowPatternData[i].file), 'slowPatternData', undefined, false)
}
