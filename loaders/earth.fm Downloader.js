// Show header
console.log('earth.fm Downloader')
console.log('')

// Create directory
require('../tools/directory')('earth.fm')

// Get recording database
console.log('Retrieving recording database...')
console.log('')
const response = require('../tools/request')('https://earth.fm/wp-admin/admin-ajax.php?action=get_sound_recordings', true)
require('fs').writeFileSync('get_sound_recordings', JSON.stringify(response))

// Do for each recording
for (let i = 0; i < response.length; i++) {
  console.log('Downloading ' + (i + 1) + '/' + response.length + ' (' + response[i].id + ')...')

  // Create directory
  require('../tools/directory')(response[i].id, undefined, false)

  // Download recording metadata
  const data = require('../tools/request')('https://earth.fm/wp-admin/admin-ajax.php?action=get_sound_recording_data&post_id=' + response[i].id, true)
  require('fs').writeFileSync(response[i].id + '/get_sound_recording_data', JSON.stringify(data))

  // Download audio
  require('../tools/download')(data.audio, undefined, response[i].id, undefined, false)

  // Download thumbnail
  require('../tools/download')(data.thumbnail.match(/([\w-./:]*?) 1600w/)[1], undefined, response[i].id, undefined, false)
}
