# Loaders

Automatic downloaders for various webpages and applications.

## Requisites

- [Node.js](https://nodejs.org/)
- [Wget](https://www.gnu.org/software/wget/)

## Installation

```
git clone https://codeberg.org/sun/loaders
cd loaders
npm install better-sqlite3
```

## Loaders

|                                                               Title | Description                                                                                                                                  |
| ------------------------------------------------------------------: | :------------------------------------------------------------------------------------------------------------------------------------------- |
|             [Abstruct Downloader](loaders/Abstruct%20Downloader.js) | Downloads all wallpapers from the [Abstruct](http://abstruct.co) collection by Hampus Olsson.                                                |
|         [Archillect Downloader](loaders/Archillect%20Downloader.js) | Downloads all images collected by [Archillect](http://archillect.com/), the synthetic intelligence.                                          |
|           [Backdrops Downloader](loaders/Backdrops%20Downloader.js) | Downloads all wallpapers from [Backdrops](https://backdrops.io/) via their semi-public API.                                                  |
|                 [Behang Downloader](loaders/Behang%20Downloader.js) | Downloads all wallpapers from [Behang](https://knokfirst.com/behang/) via their semi-public API.                                             |
|                     [Calm Downloader](loaders/Calm%20Downloader.js) | Downloads all scenes including photo, video and audio assets from [Calm](https://www.calm.com/meditate).                                     |
|       [Earth View Downloader](loaders/Earth%20View%20Downloader.js) | Downloads all images from the [Earth View](https://earthview.withgoogle.com/) collection by Google.                                          |
|             [earth.fm Downloader](loaders/earth.fm%20Downloader.js) | Downloads all recordings including metadata and thumbnail from [earth.fm](https://earth.fm/).                                                |
|                   [Emoji Downloader](loaders/Emoji%20Downloader.js) | Downloads all emotes from [Discord Emojis](https://emoji.gg/) via their semi-public API.                                                     |
|                 [Facets Downloader](loaders/Facets%20Downloader.js) | Downloads all [Facets](http://www.facets.la/) images in all available resolutions via the app's facets.db.                                   |
|             [Heritage Downloader](loaders/Heritage%20Downloader.js) | Downloads photos, snapshots, music tracks and soundscapes for every heritage from the [α CLOCK](https://www.sony.net/united/clock/) project. |
|             [Longform Downloader](loaders/Longform%20Downloader.js) | Downloads all releases from the [Longform Editions](https://longformeditions.com/) label.                                                    |
|               [Pattern Downloader](loaders/Pattern%20Downloader.js) | Downloads all patterns from the [Pattern Library](https://thepatternlibrary.com/).                                                           |
| [Quote Unquote Downloader](loaders/Quote%20Unquote%20Downloader.js) | Downloads all releases from the [Quote Unquote Records](https://quoteunquoterecords.com/) label.                                             |
|           [Snoovatar Downloader](loaders/Snoovatar%20Downloader.js) | Downloads all assets from Reddit's new [Snoo avatar builder](https://snoovatar.reddit.com/static/client/).                                   |
| [Wallpaperboard Downloader](loaders/Wallpaperboard%20Downloader.js) | Downloads all wallpapers from a [Wallpaperboard](https://github.com/danimahardhika/wallpaperboard) source.                                   |
|                     [XKCD Downloader](loaders/XKCD%20Downloader.js) | Downloads all comic images from [XKCD](https://xkcd.com/) via the JSON API.                                                                  |
